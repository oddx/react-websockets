# react-websockets
a simple websockets client and server with react

## running
To run:

```bash
nix develop
cd server
bun run index.ts
```
and to serve the client, open up a new terminal and open the html file with your browser

```bash
nix develop
cd client
bun run start
```
