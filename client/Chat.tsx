import { useEffect, useState } from 'react'
import { useWebSocket } from './useWebSockets'

const Chat = () => {
  const [ready, val, send] = useWebSocket('ws://localhost:3000')
  const [history, setHistory] = useState<string[]>([])
  const [message, setMessage] = useState<string>('')

  // useEffect(() => ready ? send('testing') : undefined, [ready, send])
  useEffect(() => setHistory([...history, val]), [val])

  const handleSubmit = (e) => {
    e.preventDefault()
    send(message)
    setMessage('')
  }

  const format = (xs: string[]) => xs.map((x, i) => <div key={i}>{x}</div>)

  return (
    <main>
      <section>
        {format(history)}
      </section>
      <form>
        <input type='text' name='message' onChange={e => setMessage(e.target.value)} value={message} />
        <input type='submit' value='send' onClick={handleSubmit} />
      </form>
    </main>
  )
}

export default Chat
